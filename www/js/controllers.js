angular.module('starter.controllers', [])

.controller('AppCtrl', function($rootScope, $scope, $state, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use laterj
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.logout = function() {
    $rootScope.loginUser = {};
    $state.go('login');

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('LoginCtrl', function($rootScope, $scope, $state, bggService, $ionicPopup, $ionicHistory) {
  $scope.$on('$ionicView.enter', function (event, viewData) {
    $ionicHistory.clearCache();
  });
  $scope.loginData = {};
  $scope.doRegister = function() {
    $state.go('register');
  };

  $scope.doLogin = function() {
    if (!($scope.loginData.username && $scope.loginData.password)) {
      $ionicPopup.alert({
        title: 'Error',
        template: 'Please key in both username and password'
      });
      return;
    }

    bggService.authenticateUser($scope.loginData).then(function(data) {
      var user = data;
      if (!user) {
        $ionicPopup.alert({
          title: 'Error',
          template: 'Wrong username or password'
        });
      }
      else {
        $rootScope.loginUser = data;
        $state.go('app.browse');
      }
    });
  };
})

.controller('RegisterCtrl', function($scope, $state, categories, $ionicHistory, bggService, $ionicPopup) {
  $scope.title = "Registration";
  $scope.submit = "Register";
  $scope.newuser = {};
  $scope.categories = categories;
  $scope.selected = []; 
  $scope.userEnabled = true;

  $scope.back = function() {
    $ionicHistory.backView().go();
  }

  $scope.doSave = function() {
    //Validating username
    if (!($scope.newuser.username && $scope.newuser.password)) {
      $ionicPopup.alert({
        title: 'Error',
        template: 'Username and Password are compulsory'
      });
      return;
    }
    //Checking for existing user
    bggService.isUserExisted($scope.newuser.username).then(function (data) {
      if (data.existed) {
        $ionicPopup.alert({
          title: 'Error',
          template: 'Username already existed. Please choose another name'
        });
      }
      else {
        //console.log($scope.newuser);
        $scope.newuser.likes = [];
        for (var i = 0; i < $scope.selected.length; i++) {
          if ($scope.selected[i])
            $scope.newuser.likes.push($scope.selected[i]);
        }
        bggService.saveUser($scope.newuser).then(function (data) {
          $ionicPopup.alert({
            title: 'Success',
            template: 'User has been registered successfully.'
          }).then(function (data) {
            $state.go('login');
          });
        });
      }
    })
  };
})

.controller('ProfileCtrl', function($rootScope, $scope, categories, $state, $ionicHistory, bggService, $ionicPopup) {
  if (!$rootScope.loginUser)
    $state.go('login');

  $scope.title = "My Profile";
  $scope.submit = "Save";
  $scope.newuser = {};
  $scope.categories = categories;
  $scope.selected = [];
  $scope.userEnabled = false;


  $scope.getUser = function(username) {
    bggService.getUser(username).then(function (data) {
      $scope.newuser = data;
    });
  };

  $scope.getUser($rootScope.loginUser.username);


  $scope.back = function() {
    $ionicHistory.backView().go();
  }

  $scope.doSave = function() {
    //Validating password
    if (!$scope.newuser.password) {
      $ionicPopup.alert({
        title: 'Error',
        template: 'Password is compulsory'
      });
      return;
    }

    //Update user
    $scope.newuser.likes = [];
    for (var i = 0; i < $scope.selected.length; i++) {
      if ($scope.selected[i])
        $scope.newuser.likes.push($scope.selected[i]);
    }
    bggService.updateUser($scope.newuser).then(function(data) {
      $ionicPopup.alert({
        title: 'Success',
        template: 'Your details has been updated successfully'
      }).then(function (data){
        //$scope.newuser = data;
      });
    });
  }
})

.controller('PostitemCtrl', function($rootScope, $scope, $state, categories, $ionicPopup, bggService) {
  if (!$rootScope.loginUser) {
    $state.go('login');
  }
  $scope.categories = categories;
  $scope.loginUsername = $rootScope.loginUser.username;
  $scope.item = {item_cat: "Electronics", belongs_to: $scope.loginUsername}

  $scope.doSave = function() {
    //Validating item name and category;
    if (!($scope.item.name && $scope.item.item_cat)) {
      $ionicPopup.alert({
        title: 'Error',
        template: 'Item name and category are compulsory'
      });
      return;
    }
    
    //Save item
    bggService.createItem($scope.item).then(function(data) {
      $ionicPopup.alert({
        title: 'Success',
        template: 'Your Item has been saved successfully'
      });
      //goto my Item
      $state.go('app.myitem');
    })
  }
})

.controller('MyitemCtrl', function($rootScope, $scope, $state, bggService) {
  if (!$rootScope.loginUser)
    $state.go('login');

  $scope.myTitle = "My Items";

  $scope.browseItem = function(username) {
    bggService.getAllMyItems(username).then(function(data) {
      $scope.items = data;
    });
  }

  $scope.browseItem($rootScope.loginUser.username);

  $scope.selectItem = function(item) {
    $state.go('app.item', {'item': item});
  }
})

.controller('ItemCtrl', function($rootScope, $scope, $state, $stateParams, $ionicModal, $ionicPopup, bggService) {
  if (!$rootScope.loginUser)
    $state.go('login');
  
  $scope.item = $stateParams.item;
  console.log($scope.item);
  $scope.ismyitem = ($scope.item.belongs_to === $rootScope.loginUser.username);
  $scope.myTitle = "Select Items";

  // Create the login modal that we will use laterj
  $ionicModal.fromTemplateUrl('templates/browse.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.browseMyItemsToTrade = function(myuser, otheruser) {
    bggService.getAllMyItemsToTrade(myuser, otheruser).then(function(data) {
      $scope.items = data;
    });
  }

  $scope.loadOffers = function() {
    bggService.getOffersByItem($scope.item._id).then(function(data) {
      console.log(data);
      $scope.offers = data;
    });
  }

  $scope.doOffer = function() {
    $scope.browseMyItemsToTrade($rootScope.loginUser.username,$scope.item.belongs_to);   
    $scope.modal.show();
  }

  $scope.selectItem = function(selecteditem) {
    var offer = { itemid: $scope.item._id, offerby: $rootScope.loginUser.username,
                  offer_itemid: selecteditem._id, status: 'Active', offer_itemname: selecteditem.name,
                  offer_itemcat: selecteditem.item_cat };
    bggService.saveOffer(offer).then(function (data) {
      $ionicPopup.alert({
        title: "Success",
        template: "Your offer has been sent."
      }).then(function (data) {
        $scope.modal.hide();
        $scope.loadOffers();
      });
    });
  }

  $scope.loadOffers();
})

.controller('BrowseCtrl', function($rootScope, $scope, $state, bggService) {
  if (!$rootScope.loginUser)
    $state.go('login');

  $scope.myTitle = "Browse Items";

  $scope.browseItem = function(username) {
    bggService.browseItems(username).then(function (data) {
      $scope.items = data;
    });
  }

  $scope.browseItem($rootScope.loginUser.username);

  $scope.selectItem = function(item) {
    $state.go('app.item', {'item': item});
  }
}) 
;