angular.module('starter.services', []) 
.factory('bggService', function($q, $http) {
    var baseUrl = "http://bgg-api.azurewebsites.net/"; 
    //var baseUrl = "http://localhost:3000/";
    var myService = {};

    myService.saveUser = function(data) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'users',
            method: 'POST',
            data: JSON.stringify(data)
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.getUser = function(username) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'users/' + username,
            method: 'GET',
            cache: false
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.isUserExisted = function(username) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'users/existed/' + username,
            method: 'GET',
            cache: false
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.authenticateUser = function(user) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'users/authenticate/' + user.username + '/' + user.password,
            method: 'GET',
            cache: false
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.updateUser = function(user) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'users/' + user.username,
            method: 'PUT',
            data: user
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.browseItems = function(username) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'items/' + username,
            method: 'GET',
            cache: false
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function (err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.getAllMyItems = function(username) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'myitems/' + username,
            method: 'GET',
            cache: false
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.getAllMyItemsToTrade = function(myusername, otheruser) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'myitems/' + myusername + '/' + otheruser,
            method: 'GET',
            cache: false
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.getItem = function(itemid) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'items/' + itemid,
            method: 'GET',
            cache: false
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.createItem = function(item) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'items',
            method: 'POST',
            data: item
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.getOffersByItem = function(itemid) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'offers/' + itemid,
            method: 'GET',
            cache: false
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.getOfferCountByItem = function(itemid) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'offers/count/' + itemid,
            method: 'GET',
            cache: false
        }).success(function (data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    myService.saveOffer = function(offer) {
        var deferred = $q.defer();
        $http({
            url: baseUrl + 'offers',
            method: 'POST',
            data: offer
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(err) {
            deferred.reject(err);
        });
        return deferred.promise;
    }

    return myService;
})